package com.epam.java.training;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

/**
 * Epam - Java Web Development Training
 * Assel Sarzhanova
 * Task 1
 */

public class App {
    public static void main( String[] args ) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String userName;
        do{
            System.out.print("Enter your name: ");
        } while((userName = bufferedReader.readLine()).trim().equals(""));

        System.out.printf("Hello %s!!!\n", userName);
        System.out.println("Enter your sequence of numbers:");

        String[] userNumbersArray = bufferedReader.readLine().split("\\s+");
        bufferedReader.close();

        BigInteger sumOfUserNumbers = new BigInteger("0");
        BigInteger productOfUserNumbers = new BigInteger("1");

        for (String userNumber : userNumbersArray) {
            sumOfUserNumbers = sumOfUserNumbers.add(BigInteger.valueOf(Long.parseLong(userNumber)));
            productOfUserNumbers = productOfUserNumbers.multiply(BigInteger.valueOf(Long.parseLong(userNumber)));

        }

        System.out.printf("Sum = %d, product = %d", sumOfUserNumbers, productOfUserNumbers);

    }
}
